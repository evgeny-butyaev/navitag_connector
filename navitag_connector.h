#ifndef NAVITAGCONNECTOR_H
#define NAVITAGCONNECTOR_H

#include <QGeoPositionInfo>
#include <QObject>
#include <QScopedPointer>
#include <QTcpSocket>
#include <QTimer>
#include <QMap>

class NavitagClientMessage;
class NavitagServerMessage;

/**
 * @brief Класс NavitagConnector
 *
 * Класс предназначен для установки, поддержки и возобновления подключения к
 * Navitag-серверу.
 *
 * Класс пытается подключится к серверу, и, в случае удачи, поддерживает с ним
 * соединение, регулярно отправляя сообщения Ping. В случае не получения ответа
 * в течении определённого временного интервала класс считает соединение разорваным
 * и пытается снова устновить подключение.
 *
 * В случае обрыва соединения класс пытается автоматически установить соединение через
 * заданные интервалы времени, до тех пор, пока соединение не будет установлени (или
 * не будет получена команда на останов от вызывающего модуля).
 *
 * Кроме этого, класс осуществляет приём, хранение и передачу на сервер геолокационных
 * данных. Класс сам не осуществляет сбор геолокационных данных, периодически сбор
 * этих данных возлагается на вызывающую программу.
 *
 * При получении очередной порции геолокационных данных класс либо отправляет её
 * на сервер немедленно, если в данный момент подключение к серверую установлено, либо
 * сохраняет во внутреннем буфере, и передаёт на сервер накопленные данные при
 * возобновлении соединения.
 *
 * Максимальный размер буфера для хранения, максимальное количество данных в
 * одном пакете, отправляемом на сервер, и прочие параметры задаются в свойствах
 * экземпляра класса (см. ниже). Значения всех свойст по умолчанию находятся в
 * файле navitag_connector.cpp
 *
 * \warning Для свойств, отвечающих за подключени (адрес сервера, идентификатор
 * устройства, и т.п.) из соображений безопасности значений по умолчанию не
 * предусмотрено - они должны задаваться вызывающим модулем.
 *
 * \warning Класс не является потокобезопастным, т.е. одновременные вызовы методов
 * одного экземпляра класса из разных потоков могу привести к непредсказуемому
 * поведению.
 */
class NavitagConnector : public QObject
{
	Q_OBJECT

public:
	enum ConnectorState
	{
		/// Коннектор не запущен или остановлен
		StoppedState,

		/// Коннектор запущен и пытается установить соединение с сервером
		ConnectingState,

		/// Коннектор запущен и подключён к серверу
		ConnectedState,
	};
	Q_ENUM(ConnectorState)

public:
	explicit NavitagConnector(QObject* parent = nullptr);
	~NavitagConnector() override;

    NavitagConnector(const NavitagConnector&) = delete;
    NavitagConnector(NavitagConnector&&) = delete;
    NavitagConnector& operator=(const NavitagConnector&) = delete;
    NavitagConnector& operator=(NavitagConnector&&) = delete;

public:
	QString host() const;
	void setHost(QString);

	quint16 port() const;
	void setPort(quint16);

	QString deviceID() const;
	void setDeviceID(QString);

	QString platform() const;
	void setPlatform(QString);

	QString version() const;
	void setVersion(QString);

	quint32 waitResponseTimeout() const noexcept;
	void setWaitResponseTimeout(quint32) noexcept;

	quint32 reconnectInterval() const noexcept;
	void setReconnectInterval(quint32) noexcept;

	quint32 pingInterval() const noexcept;
	void setPingInterval(quint32) noexcept;

	std::size_t maxQueueSize() const noexcept;
	void setMaxQueueSize(std::size_t) noexcept;

	std::size_t maxLocationsInPacket() const noexcept;
	void setMaxLocationsInPacket(std::size_t) noexcept;

	ConnectorState state() const noexcept;
	QString stateString() const;

public slots:
	void start();
	void stop();
	void enqueuePosition(QGeoPositionInfo gpi);

signals:
	void stateChanged(NavitagConnector::ConnectorState);

private:
	void process_disconnected();
	void process_connected();
	void force_disconnect();
	void restart_timer(quint32 millseconds = 0);
	void reset_socket();
	void connect_to_host();
	void drop_waitings();
	void drop_buffers();
	void send_next_data();
	void send_ping();
	void send_data();

	NavitagClientMessage prepare_client_message();
	void send_client_message(const NavitagClientMessage& cm);
	bool get_next_block(QByteArray& block);
	void process_server_message(const NavitagServerMessage& sm);

private:
	void set_host_internal(QString);
	void set_port_internal(quint16);
	void set_device_id_internal(QString);
	void set_platform_internal(QString);
	void set_version_internal(QString);

	void connection_settings_changed();

	void on_socket_connected();
	void on_socket_disconnected();
	void on_socket_error(QTcpSocket::SocketError);
	void on_socket_ready_read();
	void on_timer_timeout();

private:
	enum ConnectorStateInternal
	{
		IntStopped,
		IntConnecting,
		IntConnected,
		IntPaused,
	};
	ConnectorStateInternal state_internal_;
	ConnectorState state_;

	void set_state(ConnectorState state);
	void set_internal_state(ConnectorStateInternal int_state);
	static const char* to_string(ConnectorState s) noexcept;
	static const char* to_string(ConnectorStateInternal si) noexcept;

private:
	QString host_;
	quint16 port_;
	QString device_id_;
	QString platform_;
	QString version_;

	quint32 wait_response_;
	quint32 reconnect_interval_;
	quint32 ping_interval_;

	std::size_t max_queue_size_;
	std::size_t max_packet_size_;

	QScopedPointer<QTcpSocket> socket_;
	QByteArray received_data_;
	QTimer timer_;

	qint64 next_message_id_;
	qint64 waiting_ping_id_;
	qint64 waiting_data_id_;
	qint64 last_send_position_id_;

	QMap<qint64 , QGeoPositionInfo> queue_;
	qint64 next_queue_index_;
};

inline NavitagConnector::ConnectorState NavitagConnector::state() const noexcept
{
	return state_;
}

inline quint32 NavitagConnector::waitResponseTimeout() const noexcept
{
	return wait_response_;
}

inline void NavitagConnector::setWaitResponseTimeout(quint32 t) noexcept
{
	wait_response_ = t;
}

inline quint32 NavitagConnector::reconnectInterval() const noexcept
{
	return reconnect_interval_;
}

inline void NavitagConnector::setReconnectInterval(quint32 t) noexcept
{
	reconnect_interval_ = t;
}

inline quint32 NavitagConnector::pingInterval() const noexcept
{
	return ping_interval_;
}

inline void NavitagConnector::setPingInterval(quint32 t) noexcept
{
	ping_interval_ = t;
}

inline std::size_t NavitagConnector::maxQueueSize() const noexcept
{
	return max_queue_size_;
}

inline void NavitagConnector::setMaxQueueSize(std::size_t s) noexcept
{
	max_queue_size_ = s;
}

inline std::size_t NavitagConnector::maxLocationsInPacket() const noexcept
{
	return max_packet_size_;
}

inline void NavitagConnector::setMaxLocationsInPacket(std::size_t s) noexcept
{
	max_packet_size_ = s;
}

#endif // NAVITAGCONNECTOR_H
