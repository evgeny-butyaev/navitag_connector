#include "navitag_connector.h"

#include <QDateTime>
#include <QDebug>
#include <QMetaObject>
#include <QScopedArrayPointer>

#include <google/protobuf/util/json_util.h>

#include "tracker.pb.h"

namespace defaults
{
// Все времена в миллисекундах!
const constexpr quint32 waitResponseTimeout {500};
const constexpr quint32 reconnectInterval {5000};
const constexpr quint32 pingInterval {5000};
const constexpr std::size_t maxQueueSize {7200};
const constexpr std::size_t maxLocationsInPacket {100};
}; // namespace defaults

namespace
{
quint16 crc16_append(quint16 crc, quint8 b) noexcept
{
	crc ^= static_cast<quint16>(b) << 8;
	for (std::size_t i = 0; i < 8; ++i)
	{
		if (0 != (crc & 0x8000))
		{
			crc <<= 1;
			crc ^= 0x1021;
		}
		else
		{
			crc <<= 1;
		}
	}
	return crc;
}

quint16 crc16(const void* data, std::size_t size) noexcept
{
	const quint8* p(static_cast<const quint8*>(data));
	quint16 crc {0};

	while (size--)
	{
		crc = crc16_append(crc, *(p++));
	}

	return crc;
}
} // namespace

NavitagConnector::NavitagConnector(QObject* parent)
	: QObject(parent)
	, state_internal_ {IntStopped}
	, state_ {StoppedState}
	, wait_response_ {defaults::waitResponseTimeout}
	, reconnect_interval_ {defaults::reconnectInterval}
	, ping_interval_ {defaults::pingInterval}
	, max_queue_size_ {defaults::maxQueueSize}
	, max_packet_size_ {defaults::maxLocationsInPacket}
	, timer_ {this}
	, next_message_id_ {QDateTime::currentMSecsSinceEpoch()}
	, waiting_ping_id_ {0}
	, waiting_data_id_ {0}
	, last_send_position_id_ {0}
	, next_queue_index_ {QDateTime::currentMSecsSinceEpoch()}
{
	timer_.setSingleShot(true);
	connect(&timer_, &QTimer::timeout, this, &NavitagConnector::on_timer_timeout);

	reset_socket();
}

NavitagConnector::~NavitagConnector()
{
	timer_.stop();
	state_internal_ = IntStopped;
}

QString NavitagConnector::host() const
{
	return host_;
}

void NavitagConnector::setHost(QString host)
{
	set_host_internal(host.trimmed());
	// QMetaObject::invokeMethod(this, [this, h = host.trimmed()]() { set_host_internal(h); });
}

quint16 NavitagConnector::port() const
{
	return port_;
}

void NavitagConnector::setPort(quint16 port)
{
	set_port_internal(port);
	// QMetaObject::invokeMethod(this, [this, port]() { set_port_internal(port); });
}

QString NavitagConnector::deviceID() const
{
	return device_id_;
}

void NavitagConnector::setDeviceID(QString device_id)
{
	set_device_id_internal(device_id.trimmed());
	// QMetaObject::invokeMethod(this, [this, d = device_id.trimmed()]() { set_device_id_internal(d); });
}

QString NavitagConnector::platform() const
{
	return platform_;
}

void NavitagConnector::setPlatform(QString platform)
{
	set_platform_internal(platform.trimmed());
	// QMetaObject::invokeMethod(this, [this, p = platform.trimmed()]() { set_platform_internal(p); });
}

QString NavitagConnector::version() const
{
	return version_;
}

void NavitagConnector::setVersion(QString version)
{
	set_version_internal(version.trimmed());
	// QMetaObject::invokeMethod(this, [this, v = version.trimmed()]() { set_version_internal(v); });
}

void NavitagConnector::set_host_internal(QString host)
{
	if (host_ != host)
	{
		host_ = host;
		connection_settings_changed();
	}
}

void NavitagConnector::set_port_internal(quint16 port)
{
	if (port_ != port)
	{
		port_ = port;
		connection_settings_changed();
	}
}

void NavitagConnector::set_device_id_internal(QString device_id)
{
	if (device_id_ != device_id)
	{
		device_id_ = device_id;
		connection_settings_changed();
	}
}

void NavitagConnector::set_platform_internal(QString platform)
{
	if (platform_ != platform)
	{
		platform_ = platform;
		connection_settings_changed();
	}
}

void NavitagConnector::set_version_internal(QString version)
{
	if (version_ != version)
	{
		version_ = version;
		connection_settings_changed();
	}
}

void NavitagConnector::connection_settings_changed()
{
	qDebug() << "NavitagConnector::connection_settings_changed()";
}

void NavitagConnector::set_state(ConnectorState new_state)
{
	if (state_ == new_state)
	{
		return;
	}

	qDebug() << "State changed to: " << to_string(new_state);

	emit stateChanged((state_ = new_state));
}

void NavitagConnector::set_internal_state(ConnectorStateInternal int_state)
{
	if (state_internal_ == int_state)
	{
		return;
	}

	qDebug() << "Internal state changed to: " << to_string(int_state);

	switch ((state_internal_ = int_state))
	{
	case IntStopped:
		set_state(StoppedState);
		break;

	case IntPaused:
	case IntConnecting:
		set_state(ConnectingState);
		break;

	case IntConnected:
		set_state(ConnectedState);
		break;

	default:
		Q_ASSERT(false);
	}
}

inline const char* NavitagConnector::to_string(ConnectorState s) noexcept
{
	switch (s)
	{
	case StoppedState:
		return "StoppedState";

	case ConnectingState:
		return "ConnectingState";

	case ConnectedState:
		return "ConnectedState";

	default:
		return "<invalid>";
	}
}

inline const char* NavitagConnector::to_string(ConnectorStateInternal si) noexcept
{
	switch (si)
	{
	case IntStopped:
		return "IntStopped";

	case IntConnecting:
		return "IntConnecting";

	case IntConnected:
		return "IntConnected";

	case IntPaused:
		return "IntPaused";

	default:
		return "<invalid>";
	}
}

void NavitagConnector::start()
{
	if (IntStopped != state_internal_)
	{
		return;
	}

	connect_to_host();
}

void NavitagConnector::stop()
{
	if (IntStopped == state_internal_)
	{
		return;
	}

	restart_timer();
	reset_socket();
	set_internal_state(IntStopped);
}

void NavitagConnector::reset_socket()
{
	if (socket_)
	{
		socket_->disconnect();
		socket_->disconnectFromHost();
	}

	socket_.reset(new QTcpSocket {this});
	connect(socket_.data(), &QTcpSocket::connected, this, &NavitagConnector::on_socket_connected);
	connect(socket_.data(), &QTcpSocket::disconnected, this, &NavitagConnector::on_socket_disconnected);
	connect(socket_.data(), &QTcpSocket::readyRead, this, &NavitagConnector::on_socket_ready_read);

#if (QT_VERSION >= QT_VERSION_CHECK(5, 15, 0))
	connect(socket_.data(), &QTcpSocket::errorOccurred, this, &NavitagConnector::on_socket_error);
#else
	connect(socket_.data(), static_cast<void (QTcpSocket::*)(QTcpSocket::SocketError)>(&QTcpSocket::error),
			this, &NavitagConnector::on_socket_error);
#endif
}

QString NavitagConnector::stateString() const
{
	return QString::fromUtf8(to_string(state_));
}

void NavitagConnector::on_timer_timeout()
{
	// qDebug() << "NavitagConnector::on_timer_timeout()";
	switch (state_internal_)
	{
	case IntPaused:
		connect_to_host();
		break;

	case IntConnected:
		if ((0 != waiting_ping_id_) || (0 != waiting_data_id_))
		{
			force_disconnect();
		}
		else
		{
			send_ping();
		}
		break;

	case IntStopped:
	case IntConnecting:
	default:
		qDebug() << "Invalid internal state on timer: " << to_string(state_internal_) << "; nothing to do.";
		break;
	}
}

void NavitagConnector::force_disconnect()
{
	socket_->disconnectFromHost();
	process_disconnected();
}

void NavitagConnector::on_socket_error(QTcpSocket::SocketError)
{
	qDebug() << "NavitagConnector: Socket error: " << socket_->errorString().toUtf8().constData();
	force_disconnect();
}

void NavitagConnector::on_socket_connected()
{
	qDebug() << "NavitagConnector::on_socket_connected()";
	process_connected();
}

void NavitagConnector::on_socket_disconnected()
{
	qDebug() << "NavitagConnector::on_socket_disconnected()";
	process_disconnected();
}

void NavitagConnector::on_socket_ready_read()
{
	received_data_ += socket_->readAll();

	while (true)
	{
		QByteArray block;

		if (!get_next_block(block))
		{
			force_disconnect();
			break;
		}

		if (block.isEmpty())
		{
			// qDebug() << "Waiting for more data...";
			break;
		}

		// Try decode received block
		// qDebug() << "Received valid block of " << block.size() << " byte(s)";

		NavitagServerMessage sm;
		if (!sm.ParseFromArray(block.data(), block.size()))
		{
			qDebug() << "Error parsing block as valid protobuf message!";
			force_disconnect();
			break;
		}
		else
		{
			std::string json_message;
			google::protobuf::util::MessageToJsonString(sm, &json_message);
			qDebug() << "<<< " << json_message.c_str();
			process_server_message(sm);
		}
	}
}

void NavitagConnector::process_disconnected()
{
	if (IntStopped == state_internal_)
	{
		return;
	}

	set_internal_state(IntPaused);
	restart_timer(reconnect_interval_);
}

void NavitagConnector::process_connected()
{
	set_internal_state(IntConnected);
	drop_waitings();
	drop_buffers();
	send_next_data();
}

void NavitagConnector::restart_timer(quint32 millseconds)
{
	timer_.stop();
	if (0 != millseconds)
	{
		timer_.start(static_cast<int>(millseconds));
	}
}

void NavitagConnector::connect_to_host()
{
	Q_ASSERT(QTcpSocket::UnconnectedState == socket_->state());

	set_internal_state(IntConnecting);
	socket_->connectToHost(host_, port_);
}

void NavitagConnector::drop_waitings()
{
	waiting_ping_id_ = 0;
	waiting_data_id_ = 0;
}

void NavitagConnector::drop_buffers()
{
	received_data_.clear();
}

void NavitagConnector::send_next_data()
{
	if (queue_.isEmpty())
	{
		restart_timer(ping_interval_);
	}
	else
	{
		send_data();
	}
}

void NavitagConnector::send_ping()
{
	NavitagClientMessage cm {prepare_client_message()};
	cm.set_is_ping(true);
	waiting_ping_id_ = cm.message_id();
	send_client_message(cm);
}

NavitagClientMessage NavitagConnector::prepare_client_message()
{
	NavitagClientMessage cm;

	cm.set_deviceid(device_id_.toStdString());
	cm.set_message_id(++next_message_id_);

	auto* di(cm.mutable_device_profile());
	di->set_app_version(version_.toStdString());
	di->set_platform(platform_.toStdString());
	//di->set_supports_assistance_requests(false);

	return cm;
}

void NavitagConnector::send_client_message(const NavitagClientMessage& cm)
{
	std::string json_message;
	google::protobuf::util::MessageToJsonString(cm, &json_message);
	qDebug() << ">>> " << json_message.c_str();

	auto size(cm.ByteSizeLong());
	if ((1 > size) || (65000 < size))
	{
		qDebug() << "NavitagConnector::send_client_message(): Invalid message size: " << size << " - message discarded.";
		return;
	}

	QScopedArrayPointer<quint8> buffer(new quint8[size + 8]);
	auto* buf(buffer.data());

	if (!cm.SerializeToArray(buf + 8, static_cast<int>(size)))
	{
		qDebug() << "NavitagConnector::send_client_message(): Cannot serialize object!, message discarded.";
		return;
	}

	quint16 crc(crc16(buf + 8, size));
	quint32 usize(static_cast<quint32>(size));
	quint8* p(buf);

	// Version
	*(p++) = 0;
	*(p++) = 1;

	// Size
	*(p++) = static_cast<quint8>((usize >> 24) & 0xFF);
	*(p++) = static_cast<quint8>((usize >> 16) & 0xFF);
	*(p++) = static_cast<quint8>((usize >> 8) & 0xFF);
	*(p++) = static_cast<quint8>(usize & 0xFF);

	// crc
	*(p++) = static_cast<quint8>((crc >> 8) & 0xFF);
	*(p++) = static_cast<quint8>(crc & 0xFF);

	socket_->write(reinterpret_cast<const char*>(buf), size + 8);
	restart_timer(wait_response_);
}

bool NavitagConnector::get_next_block(QByteArray& block)
{
	block.clear();

	if (8 > received_data_.size())
	{
		return true;
	}

	if (('\0' != received_data_[0]) || ('\1' != received_data_[1]))
	{
		qDebug() << "Invalid block version.";
		return false;
	}

	quint8* p {reinterpret_cast<quint8*>(received_data_.data() + 2)};

	std::size_t size {0};
	size |= static_cast<std::size_t>(*(p++)) << 24;
	size |= static_cast<std::size_t>(*(p++)) << 16;
	size |= static_cast<std::size_t>(*(p++)) << 8;
	size |= static_cast<std::size_t>(*(p++));

	if (0x10000 < size)
	{
		qDebug() << "Size in header (" << size << ") is too big!";
		return false;
	}

	if ((size + 8) > static_cast<size_t>(received_data_.size()))
	{
		return true;
	}

	quint16 crc_in_header {0};
	crc_in_header |= static_cast<qint16>(*(p++)) << 8;
	crc_in_header |= static_cast<qint16>(*(p++));

	if (crc16(p, size) != crc_in_header)
	{
		qDebug() << "CRC16 mismatch!";
		return false;
	}

	block.append(reinterpret_cast<const char*>(p), static_cast<int>(size));
	received_data_.remove(0, static_cast<int>(size + 8));

	return true;
}

void NavitagConnector::process_server_message(const NavitagServerMessage& sm)
{
	if ((!sm.has_ack()) || (!sm.ack().has_message_id()))
	{
		return;
	}

	const qint64 ack_id {sm.ack().message_id()};

	if (ack_id == waiting_ping_id_)
	{
		waiting_ping_id_ = 0;
	}
	else if (ack_id == waiting_data_id_)
	{
		while ((!queue_.isEmpty()) && (last_send_position_id_ >= queue_.firstKey()))
		{
			queue_.remove(queue_.firstKey());
		}
		qDebug() << "NavitagConnector::process_server_message(): Queue size after ack: " << queue_.size();

		waiting_data_id_ = 0;
		last_send_position_id_ = 0;
	}
	else
	{
		qDebug() << "NavitagConnector::process_server_message(): ACK of unknown message ID: " << ack_id;
		return;
	}

	send_next_data();
}

void NavitagConnector::enqueuePosition(QGeoPositionInfo gpi)
{
	if ((!gpi.isValid()) || (!gpi.coordinate().isValid()))
	{
		qDebug() << "NavitagConnector::enqueuePosition(): invalid position passed, ignored";
		return;
	}

	queue_.insert(++next_queue_index_, gpi);
	if (static_cast<std::size_t>(queue_.size()) > max_queue_size_)
	{
		qDebug() << "Queue too big - deleting oldest position";
		queue_.remove(queue_.firstKey());
	}

	if ((IntConnected == state_internal_) && (0 == waiting_ping_id_) && (0 == waiting_data_id_))
	{
		send_next_data();
	}
}

namespace
{
inline void nav_from_gpi(TrackData::TrackPointInfo::NavInfo& ni, const QGeoPositionInfo& gpi)
{
#if (QT_VERSION >= QT_VERSION_CHECK(5, 8, 0))
	ni.set_captured_timestamp(gpi.timestamp().toSecsSinceEpoch());
#else
	ni.set_captured_timestamp(gpi.timestamp().toMSecsSinceEpoch() / 1000);
#endif

	const auto c(gpi.coordinate());
	ni.set_lat_deg(c.latitude());
	ni.set_lon_deg(c.longitude());
	ni.set_altitude_m(c.altitude());

	if (gpi.hasAttribute(QGeoPositionInfo::HorizontalAccuracy))
	{
		ni.set_accuracy_m(gpi.attribute(QGeoPositionInfo::HorizontalAccuracy));
	}

	if (gpi.hasAttribute(QGeoPositionInfo::GroundSpeed))
	{
		ni.set_speed_kmh(gpi.attribute(QGeoPositionInfo::GroundSpeed) * 3.6f);
	}

	if (gpi.hasAttribute(QGeoPositionInfo::Direction))
	{
		ni.set_direction_deg(gpi.attribute(QGeoPositionInfo::Direction));
	}
}
} // namespace

void NavitagConnector::send_data()
{
	if (queue_.empty())
	{
		qDebug() << "NavitagConnector::send_data(): invalid call with empty queue";
		return;
	}

	qint64 first_id {queue_.firstKey()};
	qint64 last_id {queue_.lastKey()};
	Q_ASSERT((last_id - first_id + 1) == queue_.size());

	auto cm(prepare_client_message());
	waiting_data_id_ = cm.message_id();

	for (qint64 i = 0; i < static_cast<qint64>(max_packet_size_); ++i)
	{
		const auto it(queue_.constFind(first_id + i));
		if (queue_.constEnd() == it)
		{
			break;
		}

		last_send_position_id_ = it.key();
		nav_from_gpi(*cm.add_track_point_info()->mutable_nav_info(), it.value());
	}

	send_client_message(cm);
}
